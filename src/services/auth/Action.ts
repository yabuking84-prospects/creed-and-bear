
import { waits } from '@/utils/helpers'

import { faker } from '@faker-js/faker'

export class Action {
  static async login(email: string) {
    return await Action.loginAPI(email)
  }

  private static async loginAPI(email: string) {
    // fake api
    if (email !== 'superuser@creedandbear.com')
      throw new Error(`Testing phase, try superuser@creedandbear.com`)
    
    await waits(2000)
    const user = {
      id: 1,
      email: 'superuser@creedandbear.com',
      first_name: faker.person.firstName(),
      last_name: faker.person.lastName(),
      avatar: faker.image.avatar()
    }
    /////////////////////////////////

    return {
      status: 'success',
      user: {
        ...user
      }
    }
  }
}
