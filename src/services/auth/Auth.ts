import { useStore as useAuthStore } from '@/stores/auth'
import { useStore as useUsersStore } from '@/stores/users'
import { UserSchema } from '@/schemas/user'

import { useStore } from '@/stores/auth'
import { Action } from './Action'
export class Auth {

  static get data() {
    const store = useStore()
    return store.user
  }

  static get isUserLoggedIn() {
    const store = useStore()
    return !!store.user?.isLoggedIn
  }

  static async logout() {
    const authStore = useAuthStore()
    authStore.user = undefined

    const usersStore = useUsersStore()
    usersStore.users = []
  }

  static async login(email: string) {
    UserSchema.shape.email.parse(email)

    const res = await Action.login(email)

    if (res.status == 'success') {
      const authStore = useAuthStore()
      authStore.user = {
        ...res.user,
        isLoggedIn: true
      }
    } else {
      throw new Error(`Invalid Credentials!`)
    }
  }  
}
