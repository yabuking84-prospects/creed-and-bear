import { useStore } from '@/stores/users'
import { CreateUserSchema } from '@/schemas/user'
import { waits } from '@/utils/helpers'

import { faker } from '@faker-js/faker'

export class Action {

  static async create(newUser: { first_name: string; last_name: string; email: string }) {
    const parsed = CreateUserSchema.parse(newUser)
    const res = await Action.createAPI(parsed)
    const store = useStore()
    store.users = [
      {
        id: res.user.id,
        email: res.user.email,
        first_name: res.user.first_name,
        last_name: res.user.last_name,
        avatar: res.user.avatar
      },
      ...store.users
    ]
  }

  private static async createAPI(newUser: {
    first_name: string
    last_name: string
    email: string
  }) {
    // fake api
    await waits(2000)
    const user = {
      id: faker.number.int({ min: 100, max: 200 }),
      email: newUser.email,
      first_name: newUser.first_name,
      last_name: newUser.last_name,
      avatar: faker.image.avatar()
    }
    /////////////////////////////////

    return {
      status: 'success',
      user: {
        ...user
      }
    }
  }

  static async edit(user: { first_name: string; last_name: string; email: string }) {
    return await Action.editAPI(user)
  }
  private static async editAPI(user: { first_name: string; last_name: string; email: string }) {
    // fake api
    await waits(2000)
    return {
      status: 'success'
    }
    /////////////////////////////////
  }


  static async delete(userId: number) {
    return await Action.deleteAPI(userId)
  }

  private static async deleteAPI(userId: number) {
    // fake api
    await waits(2000)
    return {
      status: 'success'
    }
    /////////////////////////////////
  }
    
}
