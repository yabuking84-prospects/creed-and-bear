import { UserSchema, type UserType } from '@/schemas/user'

import { faker } from '@faker-js/faker'

import { waits } from '@/utils/helpers'

export class Fetch {
  static async userById(userId: UserType['id']) {
    const res = await Fetch.userByIdAPI(userId)
    const parsed = UserSchema.parse(res.user)
    return { ...parsed }
  }

  private static async userByIdAPI(userId: UserType['id']) {
    // fake api
    await waits(2000)
    const user = {
      id: userId,
      email: faker.internet.email(),
      first_name: faker.person.firstName(),
      last_name: faker.person.lastName(),
      avatar: faker.image.avatar()
    }
    /////////////////////////////////

    return {
      user: user
    }
  }

}
