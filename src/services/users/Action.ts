import { useStore } from '@/stores/users'
import { type UserType } from '@/schemas/user'
import { waits } from '@/utils/helpers'

export class Action {
  static async deleteUsers(usersId: UserType['id'][]) {
    const res = await Action.deleteUsersAPI(usersId)

    // delete from store
    if (res.status == 'success' && usersId.length) {
      const store = useStore()
      const newUsers = store.users.filter(e=>!usersId.includes(e.id))
      store.users = [...newUsers]
    }
  }

  private static async deleteUsersAPI(usersId: UserType['id'][]) {
    // fake api
    await waits(2000)
    return {
      status: 'success'
    }
    /////////////////////////////////
  }
}
