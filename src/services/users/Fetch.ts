import { UsersSchema, type UserType } from '@/schemas/user'

import { faker } from '@faker-js/faker'

import { Users } from '@/services/users/Users'
import { useFetch } from '@vueuse/core'
import { computed, type Ref } from 'vue'

export class Fetch {
  static createUseFetchUsers(options: { page: Ref<number> }) {
    const url = computed(() => {
      // return 'https://httpbin.org/get?page=' + options.page.value
      return 'https://httpbin.org/delay/2?page=' + options.page.value
      // return 'http://httpbin.org/status/500?' + options.page.value // simulate error url
    })
    return useFetch<{
      users: UserType[]
      page: number
      total: number
      total_pages: number
    }>(url, {
      onFetchError(ctx) {
        return ctx
      },
      async afterFetch(ctx) {
        if (ctx.response.ok) {
          // fake DATA
          const users: UserType[] = faker.helpers.multiple(
            () => ({
              id: faker.number.int({ min: 2, max: 5000 }),
              email: faker.internet.email(),
              first_name: faker.person.firstName(),
              last_name: faker.person.lastName(),
              avatar: faker.image.avatar()
            }),
            {
              count: 12
            }
          )

          // users[3] = {
          //   idsss: faker.number.int({ min: 2, max: 100 }),
          //   email: faker.internet.email(),
          //   first_name: faker.person.firstName(),
          //   last_name: faker.person.lastName(),
          //   avatar: faker.image.avatar()
          // }

          ctx.data = {
            page: options.page.value,
            total: 120,
            total_pages: 10
          }
          /////////////////////////////////

          ctx.data.users = UsersSchema.parse(users)
        }
        return ctx
      },
      refetch: true,
      immediate: false
    })
  }
}
