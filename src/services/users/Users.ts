import { useStore } from '@/stores/users'
import type { UserType } from '@/schemas/user'
import { User } from '@/services/user/User'

export class Users {
  static get data() {
    const store = useStore()
    return store.users
  }

  static get dataAsNewUsers(): User[] {
    return Users.dataAsNewUsersById()
  }

  static dataAsNewUsersById(usersId?: UserType['id'][]) {
    const store = useStore()
    const retVal: User[] = []
    if (!usersId?.length) {
      const uniqueUsers = store.users.filter((item, pos) => store.users.findIndex(i=>i.id==item.id) == pos)
      uniqueUsers?.forEach((e) => {
        retVal.push(new User(e.id))
      })
    } else {
      const arr = store.users.map((e) => e.id).filter((id) => usersId.includes(id))
      const uniqueArr = arr.filter((item, pos) => arr.indexOf(item) == pos)
      uniqueArr.forEach((id) => {
        retVal.push(new User(id))
      })
    }
    return retVal
  }

  static get hasUsers() {
    const store = useStore()
    return store.users?.length
  }

  static setUsersToStore(users: UserType[]) {
    const store = useStore()
    store.users = [...users]
  }
}
