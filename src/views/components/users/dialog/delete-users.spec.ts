import { createTestingPinia } from '@pinia/testing'
import { DOMWrapper, flushPromises, mount } from '@vue/test-utils'
import { afterEach, beforeEach, describe, expect, test, vi, type Mock } from 'vitest'

import { User } from '@/services/user/User'
import { Users } from '@/services/users/Users'
import { Action as UsersAction } from '@/services/users/Action'
import Component from './delete-users.vue'
import MyButton from '@/plugins/base/ui/MyButton.vue'
import MyInput from '@/plugins/base/ui/MyInput.vue'
import * as useToastExports from '@/views/components/shadcn/ui/toast/use-toast'
import { computed } from 'vue'

interface TestContext {
  wrapper: ReturnType<typeof mount>
  btnYes: DOMWrapper<Element>
  btnNo: DOMWrapper<Element>
  toast: Mock<any, any>
}

const users = [
  {
    id: 123,
    email: 'email@address.com',
    first_name: 'Megah',
    last_name: 'Posseh',
    avatar: 'https://someimage.com/image111.jpg'
  },  
  {
    id: 222,
    email: 'email222@address.com',
    first_name: 'NAme 222',
    last_name: 'Posseh 222',
    avatar: 'https://someimage.com/image222.jpg'
  },  
  {
    id: 333,
    email: 'asd333@address.com',
    first_name: 'Yoo 333',
    last_name: 'Jiko 333',
    avatar: 'https://someimage.com/image333.jpg'
  },  
]


describe('DeleteUsers.vue', () => {
  beforeEach<TestContext>(async (context) => {
    context.toast = vi.fn()
    vi.spyOn(Users, 'dataAsNewUsersById').mockImplementation((usersId?: number[]) => {
      return [
        new User(users[0]),
        new User(users[1]),
        new User(users[2]),
      ]
    })

    vi.spyOn(useToastExports, 'useToast').mockImplementation(() => {
      return {
        toast: context.toast,
        dismiss: vi.fn(),
        toasts: computed(() => [])
      }
    })

    context.wrapper = mount(Component, {
      global: {
        plugins: [
          createTestingPinia({
            createSpy: vi.fn
          })
        ],
        renderStubDefaultSlot: true,
        stubs: {
          teleport: true,
          MyButton: MyButton,
          MyInput: MyInput
        }
      },
      props: {
        usersId: [
          users[0].id,
          users[1].id,
          users[2].id,
        ]
      }
    })
    await context.wrapper.setProps({ action: 'delete-users' })
    context.btnYes = context.wrapper.find('[data-testid="yes-btn"]')
    context.btnNo = context.wrapper.find('[data-testid="no-btn"]')
    await flushPromises()
  })

  afterEach(() => {
    vi.clearAllMocks()
    vi.resetAllMocks()
  })

  test<TestContext>('Error: press yes', async ({ wrapper, btnYes, toast }) => {
    vi.spyOn(UsersAction as any, 'deleteUsersAPI').mockImplementationOnce(() => {
      throw new Error('Error deleting users!')
    })

    await btnYes.trigger('click')
    await flushPromises()
    expect(wrapper.html()).toContain('Error deleting users!')
  })

  test<TestContext>('renders properly', async ({ wrapper }) => {
    expect(wrapper.html()).toContain('Delete Multiple Users')
    for(let i=0;i<2;i++) {
      expect(wrapper.html()).toContain(users[i].first_name)
      expect(wrapper.html()).toContain(users[i].last_name)
      expect(wrapper.html()).toContain(users[i].email)
    }

  })

  test<TestContext>('press no', async ({ wrapper,  btnNo, toast }) => {
    const mockDeleteUsers = vi.spyOn(UsersAction, 'deleteUsers').mockResolvedValueOnce()
    await btnNo.trigger('click')
    await flushPromises()
    expect(wrapper.html()).not.toContain('Delete Multiple Users')
    expect(mockDeleteUsers).not.toBeCalled()
    expect(toast).not.toBeCalledWith({
      title: 'User deleted!',
    })      
  })

  test<TestContext>('press yes', async ({ wrapper, btnYes, toast }) => {
    const mockDeleteUsers = vi.spyOn(UsersAction, 'deleteUsers').mockResolvedValueOnce()
    await btnYes.trigger('click')
    await flushPromises()
    expect(wrapper.html()).not.toContain('Delete Multiple Users')
    expect(mockDeleteUsers).toBeCalledWith([
      users[0].id,
      users[1].id,
      users[2].id,
    ])
    expect(toast).toBeCalledWith({
      title: 'Users deleted!',
    })    
  })


})
